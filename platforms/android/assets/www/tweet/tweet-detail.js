define(['text!../tweet/tweet-detail.html', "../base/openapi", '../base/util', '../base/login/login', '../base/bigImg/bigImg'],
	function(viewTemplate, OpenAPI, Util, Login, BigImg) {
		return Piece.View.extend({
			id: 'tweet_tweet-detail',
			login: null,
			bigImg: null,
			events: {
				"click .tweetListissuer": "gotoUser",
				"click .emotion": "ins_e",
				"click #showEmotion": "showEmotion",
				"click #commentTweet": "commentTweet",
				"click #showIssue": "showIssue",
				"click .refreshBtn": "refreshTweetDetail",
				"click .tweetContent": "goToReply",
				"click .backBtn": "goBack",
				"click .atwho": "atwho",
				"focus #commentCont": "focusCom",
				"click .tweetListImg": 'goToUserInfor',
				"click .listImg": "bigImg" //点击图片查看大图
			},
			bigImg: function(el) {
				Util.bigImg(el);
			},
			goToUserInfor: function(imgEl) {
				Util.imgGoToUserInfor(imgEl);
			},
			//simona--modify--input获得焦点的时间表情收起
			focusCom: function() {
				var dataSign = $("#showEmotion").attr("data-sign");
				if (dataSign === "down") {
					this.showEmotion();
				}
			},
			atwho: function(el) {
				var me = this;
				var $target = $(el.currentTarget);
				var id = $target.attr("data-ident");
				console.info(id)
				Util.Ajax(OpenAPI.user_information, "GET", {
					friend: id,
					dataType: 'jsonp'
				}, 'json', function(data, textStatus, jqXHR) {
					var author = data.name;
					author = encodeURI(author);
					var authorid = data.user;
					me.navigateModule("common/common-seeUser?" + "fromAuthor=" + author + "&fromAuthorId=" + authorid, {
						trigger: true
					});
				}, null);

			},
			gotoUser: function(el) {
				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var author = $target.attr("data-author");
				author = encodeURI(author);
				var authorid = $target.attr("data-authorid");

				this.navigateModule("common/common-seeUser?id=" + id + "&fromAuthor=" + author + "&fromAuthorId=" + authorid, {
					trigger: true
				});

			},
			ins_e: function(el) {
				$target = $(el.currentTarget);
				var num = $target.attr("data-num");
				$("#commentCont").insertContent(num);
			},
			showEmotion: function() {
				$("#TweetEmotions").toggle();
				var dataSign = $("#showEmotion").attr("data-sign");
				if (dataSign === "up") {
					$("#bar-tab2").attr({
						"style": "position:fixed;bottom:170px;display:block"
					});
					$("#showEmotion").attr({
						"data-sign": "down"
					});
					var myScroll = new iScroll("TweetEmotions", {
						checkDOMChanges: true
					});

				} else {
					$("#bar-tab2").attr({
						"style": "display:block"
					});
					$("#showEmotion").attr({
						"data-sign": "up"
					});
				}

			},
			commentTweet: function() {
				Util.checkLogin();
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					login.show();
				} else {
					this.commentPub();
				}
			},
			commentPub: function() {
				var commentCont = $("#commentCont").val();
				var me = this;
				commentCont = commentCont.replace(/(\s*$)/g, ""); //去掉右边空格
				if (commentCont === "" || commentCont === " ") {
					new Piece.Toast('请输入评论内容');
				} else {
					var userToken = Piece.Store.loadObject("user_token");
					var accessToken = userToken.access_token;
					var tweetId = Util.request("id");
					Util.Ajax(OpenAPI.comment_pub, "GET", {
						"access_token": accessToken,
						"id": tweetId,
						"catalog": 3,
						"content": commentCont,
						"dataType": 'jsonp'
					}, 'json', function(data, textStatus, jqXHR) {
						//评论之后的一些评论框页面效果及刷新动态数据
						$("#bar-tab1").show();
						$("#bar-tab2").hide();
						$("#TweetEmotions").hide();
						$("#bar-tab2").attr({
							"style": "none"
						});
						$("#showEmotion").attr({
							"data-sign": "up"
						});
						new Piece.Toast('评论成功');
						me.onShow();
					}, null, null);

				}
			},
			showIssue: function() {
				$("#bar-tab1").hide();
				$("#bar-tab2").show();
			},
			refreshTweetDetail: function() {
				this.onShow();
			},
			goToReply: function(el) {
				var $target = $(el.currentTarget);
				var tweetId = Util.request("id");
				var commentAuthorId = $target.attr("data-commentAuthorId");
				var commentId = $target.attr("data-commentId");
				var dataSource = "cube-list-tweet-reply-list";
				//事件冒泡处理
				if (Util.eventBubble(el) == false) {
					return;
				}
				if (tweetId == commentId) {
					return; // 点击发贴人，不用跳转
				}
				this.navigateModule("common/comment-reply?tweetId=" + tweetId + "&commentAuthorId=" + commentAuthorId + "&commentId=" + commentId + "&dataSource=" + dataSource + "&catalog=3", {
					trigger: true
				});
			},
			goBack: function() {
				history.back();
			},
			render: function() {
				login = new Login();
				bigImg = new BigImg();
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				this.initEmotions();
				this.initDetail();
				if (navigator.userAgent.indexOf("MI-ONE") > -1) {
					$("#commentTweet").attr('style', 'height:40px !important;margin-top:4px;');

				}
				//write your business logic here :)
			},
			initEmotions: function() {
				// var emotions = new Array();
				// for (var i = 0; i < 135; i++) {
				// 	var emotion = {};
				// 	emotion.num = "[" + i + "]";
				// 	emotion.position = -24 * i;
				// 	emotions.push(emotion);
				// }
				var emotions = new Array();
				for (var i = 1; i <= 105; i++) {
					var emotion = {};
					// emotion.num = "[" + i + "]";
					//----simona--modify--
					if (i === 66 || i === 102) {
						continue;
					}
					var num = "";

					if (i < 10) {
						num = "00" + i;
					} else if (i >= 10 && i < 100) {
						num = "0" + i;
					} else {
						num = i;
					}
					emotion.alias = num;
					// console.info(alias)
					emotion.num = "[" + (i - 1) + "]";
					emotion.position = -24 * i;
					emotions.push(emotion);
				}
				var data = {
					"emotions": emotions
				};
				var emotionsTemplate = $(this.el).find("#emotionsTemplate").html();
				var emotionsHtml = _.template(emotionsTemplate, data);
				$("#TweetEmotions").html("");
				$("#TweetEmotions").append(emotionsHtml);
			},
			initDetail: function() {
				var me = this;
				var id = Util.request("id");
				//1 第一次进入没有缓存，请求动弹详情第一条列表数据
				if (window.localStorage["cube-list-tweet-reply-list"] === undefined || window.localStorage["cube-list-tweet-reply-list"] === null) {
					Util.Ajax(OpenAPI.tweet_detail, "GET", {
						id: id,
						dataType: 'jsonp'
					}, 'json', function(data, textStatus, jqXHR) {
						//第一条动弹详情与详情回复列表 合并。
						// (&) == ,     (^)==:  
						if (data.imgSmall) { //是否有图片。
							var detailData = "id(^)" + data.id + "(&)" + "pubDate(^)" + data.pubDate + "(&)" + "content(^)" + data.body + "(&)" + "commentAuthor(^)" + data.author + "(&)" + "commentAuthorId(^)" + data.authorid + "(&)" + "commentCount(^)" + data.commentCount + "(&)" + "commentPortrait(^)" + data.portrait + "(&)" + "imgSmall(^)" + data.imgSmall + "(&)" + "imgBig(^)" + data.imgBig;
						} else {
							var detailData = "id(^)" + data.id + "(&)" + "pubDate(^)" + data.pubDate + "(&)" + "content(^)" + data.body + "(&)" + "commentAuthor(^)" + data.author + "(&)" + "commentAuthorId(^)" + data.authorid + "(&)" + "commentCount(^)" + data.commentCount + "(&)" + "commentPortrait(^)" + data.portrait + "(&)";
						}
						window.sessionStorage["initDetail"] = detailData;
					}, null, function(xhr, status) {
						me.initReply();
					});
				} else {
					//2 如果有缓存 ，则判断ID是否相同。相同则拿缓存，否则重新请求。
					var oldList = window.localStorage["cube-list-tweet-reply-list"];
					var fristDetail = new Object();
					    fristDetail = oldList.split(",");
					var detailFrist = fristDetail[0].split(":");
					var fristID = detailFrist[1];
						fristID = fristID.replace(/\"/g, "");
					if (fristID === id) {
						me.initReply();
					} else {
						Util.Ajax(OpenAPI.tweet_detail, "GET", {
							id: id,
							dataType: 'jsonp'
						}, 'json', function(data, textStatus, jqXHR) {
							if (data.imgSmall) { //是否有图片。
								//拼接对象
								var detailData = "id(^)" + data.id + "(&)" + "pubDate(^)" + data.pubDate + "(&)" + "content(^)" + data.body + "(&)" + "commentAuthor(^)" + data.author + "(&)" + "commentAuthorId(^)" + data.authorid + "(&)" + "commentCount(^)" + data.commentCount + "(&)" + "commentPortrait(^)" + data.portrait + "(&)" + "imgSmall(^)" + data.imgSmall + "(&)" + "imgBig(^)" + data.imgBig;
							} else {
								var detailData = "id(^)" + data.id + "(&)" + "pubDate(^)" + data.pubDate + "(&)" + "content(^)" + data.body + "(&)" + "commentAuthor(^)" + data.author + "(&)" + "commentAuthorId(^)" + data.authorid + "(&)" + "commentCount(^)" + data.commentCount + "(&)" + "commentPortrait(^)" + data.portrait + "(&)";
							}
							window.sessionStorage["initDetail"] = detailData;
						}, null, function(xhr, status) {
							me.initReply();//完成后也要请求回复的列表
						});
					}
				}

			},
			initReply: function() {
				var me = this;
				var id = Util.request("id");
				Util.loadTweetDetailList(this, 'tweet-reply-list', OpenAPI.comment_list, {
					'id': id,
					'catalog': 3,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				}, "tweet-reply-list", id);
			}

		}); //view define

	});