define(['text!../news/news-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'news_news—list',
			events: {
				"click .newsList": "goToNewsDetail",
				"click .right-banner": "goNewsSearch"
			},

			goToNewsDetail: function(el) {
				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var from = $('.active').attr('data-value').split('/')[1];
				var type = 4;
				var checkDetail = "news/news-detail";
				var comment = 1;
				this.navigate("news-detail?id=" + id + "&from=" + from + "&fromType=" + type + "&checkDetail=" + checkDetail + "&com=" + comment, {
					trigger: true
				});
			},
			goNewsSearch: function() {
				/*跳转到搜索页面，如果localstorage有值 则先删除，防止其从localstorage获取搜索条件*/
				var softwareSearch = Piece.Store.loadObject("software-search");
				if (softwareSearch) {
					Piece.Store.deleteObject("software-search");
				}
				this.navigate("news-search", {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				Util.loadList(this, 'news-news-list', OpenAPI.news_list, {
					'catalog': 3,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				});
				this.initEvent();
				Util.roundRobin();
				//判断网络是否连接
				this.networkState();

				$("a").die("click", Util.liveATagEvent);
				$("a").live("click", Util.liveATagEvent);

				$("input").live("keydown", function(e) {
					e = event.keyCode;
					if (e == 13) {
						event.returnValue = false;
					}

				})
				if (window.device != undefined) {
					if (parseFloat(window.device.version) === 7.0) {
						document.body.style.marginTop = "20px";
					}
				}

			},

			//点击返回键执行事件
			onBackKeyDown: function() {
				// alert('backbutton');

				var onConfirm = function(buttonIndex) {
					if (buttonIndex == 2) {
						navigator.app.exitApp();
					}
				};
				
				if (typeof $('#loginContentMasker').get(0) !== 'undefined') {
					if ($('#loginContentMasker').get(0).style.display == 'block') {
						// alert('login');
						// login.hide();
						$('#loginContentMasker').hide();
						$('#loginContent').hide();
						if ($('#footer-more-detail').get(0).style.display == "block") {
							$("#more-detail-mask").show();
						}

						return;
					}

				}

				if (typeof $('#footer-more-detail').get(0) !== 'undefined') {
					if ($('#footer-more-detail').get(0).style.display == "block") {

						// alert('detial hide');
						$('#footer-more-detail').hide();
						$("#more-detail-mask").hide();
						return;
					}

				}


				//新闻评论判断
				// alert(navigate.app);


				if (typeof $(".commentBtn").get(0) !== 'undefined') {
					if ($(".commentBtn").get(0).style.display !== '' && $(".commentBtn").get(0).style.display !== 'none') {
						// alert('comment');
						$('.tab-inner').find('.tab-item').show();
						$('.tab-inner').find('.commentBtn').hide();
						$('.footcomment').blur();
						$('.footcomment').attr("placeholder", "发表评论");
						return;

					}
				}

				if (typeof $("#bar-tab2").get(0) !== 'undefined') {
					if ($("#bar-tab2").get(0).style.display !== "" && $("#bar-tab2").get(0).style.display !== "none") {
						$("#bar-tab2").hide();
						$("#bar-tab1").show();
						return;
					}
				}

				//关闭查看大图
				if (typeof $("#imgMasker").get(0) !== 'undefined') {
					if ($('#imgMasker').get(0).style.display == 'block') {
						$('.imgContent').hide();
						$('.button-container').hide();
						$('#imgMasker').hide();
						return;
					}

				}

				// alert("back");
				if (typeof $('.footer-menu').get(0) !== 'undefined') {
					navigator.notification.confirm(
						'确定退出程序吗？', // message
						onConfirm, // callback to invoke with index of button pressed
						'提示', // title
						'取消, 确定' // buttonLabels
					);
					return;

				}



				//如果是在开源软件的5个模块的首页，那么跳回上一层
				var fulUrl = window.location.href;
				var url = fulUrl.split("#");
				if (url[1] == "project/software-list" || url[1] == "project/recommend-list" ||
					url[1] == "project/newest-list" || url[1] == "project/hot-list" || url[1] == "project/domestic-list" ||
					url[1] == "user/user-info") {
					var lastPage = Piece.Session.loadObject("osLastPage");
					Backbone.history.navigate(lastPage, {
						trigger: true
					});
					return;
				}

				window.history.back();

			},
			onOffline: function() {
				new Piece.Toast("网络连接失败，请检查网络设置");
			},


			initEvent: function() {
				//绑定android点击返回键按钮事件。
				document.addEventListener("backbutton", this.onBackKeyDown, false);
				//绑定offline事件
				document.addEventListener("offline", this.onOffline, false);
				//绑定android点击菜单按钮事件
				document.addEventListener("menubutton", this.onmenuKeydown, false);
				//绑定android点击搜索按钮事件
				document.addEventListener("searchbutton", this.onSearchKeyDown, false);
			},
			onmenuKeydown: function() {
				if (typeof $('.footer-menu').get(0) !== 'undefined') {
					if ($('#footer-more-detail').get(0).style.display == "block") {
						$('#footer-more-detail').hide();
						$("#more-detail-mask").hide();
					} else {
						$('#footer-more-detail').show();
						$("#more-detail-mask").show();
					}
					var user_token = Piece.Store.loadObject("user_token");
					if (!user_token || user_token === null) {
						$(".footSign").html("");
						$(".footSign").html("<span class='icon-user icon-2x'></span><div>用户登录</div>");
						$(".footSign").attr({
							"data-sign": "signin"
						});
					} else {
						$(".footSign").html("");
						$(".footSign").html("<span class='icon-power-off icon-2x'></span><div>注销登录</div>");
						$(".footSign").attr({
							"data-sign": "signout"
						});
					}
				}
			},
			onSearchKeyDown: function() {
				if (typeof $('.footer-menu').get(0) !== 'undefined') {
					Backbone.history.navigate("news/news-search", {
						trigger: true
					});
					return;

				}
			},
			networkState: function() {
				if (navigator.connection) {
					var networkState = navigator.connection.type;
					var appfrist = window.sessionStorage.AppFrist;

					var states = {};
					states[Connection.UNKNOWN] = 'Unknown connection';
					states[Connection.ETHERNET] = 'Ethernet connection';
					states[Connection.WIFI] = 'WiFi connection';
					states[Connection.CELL_2G] = 'Cell 2G connection';
					states[Connection.CELL_3G] = 'Cell 3G connection';
					states[Connection.CELL_4G] = 'Cell 4G connection';
					states[Connection.NONE] = 'No network connection';
					if (appfrist == 0 && states[networkState] == "No network connection") {
						new Piece.Toast('网络连接失败，请检查网络设置');
						window.sessionStorage.AppFrist = 1;
					}

				}

			}

		}); //view define

	});