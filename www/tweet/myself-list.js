define(['text!../tweet/myself-list.html', '../base/openapi', '../base/util', '../base/login/login', '../base/bigImg/bigImg', '../base/caretInsert/jquery.hammer.min'],
	function(viewTemplate, OpenAPI, Util, Login, BigImg) {
		return Piece.View.extend({
			id: 'tweet_myself-list',
			TimeFn: null,
			bigImg: null,
			events: {
				// "dblclick .tweetList": "deleteTweet",
				// "click .tweetContent": "goToMyselfDetail",
				"click .tweetText": "goToMyselfDetail",
				"click .issueBtn": "goToIssueTweet",
				"click .atwho": "atwho",
				"click .tweetListImg": "goToUserInfor",
				"click .listImg": "bigImg"
			},
			bigImg: function(el) {
				Util.bigImg(el);
			},
			goToUserInfor: function(imgEl) {
				//如果confirm出现，那么return，不进入个人信息。
				if (typeof $('.cube-dialog-screen').get(0) != 'undefined') {
					return;
				};
				Util.imgGoToUserInfor(imgEl);
			},
			atwho: function(el) {
				var me = this;
				var $target = $(el.currentTarget);
				var id = $target.attr("data-ident");
				Util.Ajax(OpenAPI.user_information, "GET", {
					friend: id,
					dataType: 'jsonp'
				}, 'json', function(data, textStatus, jqXHR) {
					var author = data.name;
					author = encodeURI(author);
					var authorid = data.user;
					me.navigateModule("common/common-seeUser?" + "fromAuthor=" + author + "&fromAuthorId=" + authorid, {
						trigger: true
					});
				}, null);

			},
			goToMyselfDetail: function(el) {
				//如果confirm出现，那么return，不进入详情。
				if (typeof $('.cube-dialog-screen').get(0) != 'undefined') {
					return;
				};
				var me = this;
				clearTimeout(TimeFn);
				TimeFn = setTimeout(function() {
					var $target = $(el.currentTarget);
					var id = $target.attr("data-id");
					//事件冒泡处理
					if (Util.eventBubble(el) == false) {
						return;
					}
					me.navigate("tweet-detail?id=" + id, {
						trigger: true
					});
				}, 300);
			},
			deleteTweet: function(el) {
				clearTimeout(TimeFn);
				var me = this;
				var dialog = new Piece.Dialog({
					autoshow: false,
					target: 'body',
					title: '确定删除此动弹？',
					content: ''
				}, {
					configs: [{
						title: '确认',
						eventName: 'ok'
					}, {
						title: '取消',
					}],
					ok: function() {
						//检查登陆
						var checkLogin = Util.checkLogin();
						if (checkLogin === false) {
							var login = new Login();
							login.show();
							return;
						}
						var that = me;
						var userToken = Piece.Store.loadObject("user_token");
						var accessToken = userToken.access_token;
						var $target = $(el.currentTarget);
						console.info($target);
						var id = $target.attr("data-id");
						// console.info(id);
						Util.Ajax(OpenAPI.tweet_delete, "GET", {
							access_token: accessToken,
							tweetid: id,
							dataType: 'jsonp'
						}, 'json', function(data, textStatus, jqXHR) {
							if (data.error === "200") {
								new Piece.Toast("删除成功");
								// that.onShow();
								Util.reloadPage('tweet/myself-list')
							} else {
								new Piece.Toast(data.error_description);
							}
						}, null, null);
					}
				});

				dialog.show();
				$('.cube-dialog-screen').click(function() {
					dialog.hide();
				})

			},
			goToIssueTweet: function() {
				var from = "myself-list";
				this.navigate("tweet-issue?from=" + from, {
					trigger: true
				});
			},
			render: function() {
				TimeFn = null;
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				// tweet-myself-list
				bigImg = new BigImg();
				// Util.checkLogin();
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					var login = new Login();
					login.show();
				} else {
					this.showMyTweet();
				}
				//计算图片右边空白的div的宽度
				var blankWidth = $(".tweetContent").width() - 100;
				console.info(blankWidth);
				$(".blankDiv").width(blankWidth);

				//绑定长按
				var me = this;
				me.longTouch(me);

				//write your business logic here :)
			},
			showMyTweet: function() {
				var user_message = Piece.Store.loadObject("user_message");
				Util.loadList(this, 'tweet-myself-list', OpenAPI.tweet_list, {
					'user': user_message.id,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				}, true);

			},
			longTouch: function(me) {
				var hammertime = $(me.el).find("#tweet-myself-list").hammer();
				// console.info(hammertime);
				// on elements in the toucharea, with a stopPropagation
				hammertime.on("hold", ".tweetList", function(ev) {
					me.deleteTweet(ev);
				});

			}
		}); //view define

	});